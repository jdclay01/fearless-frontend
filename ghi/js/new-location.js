window.addEventListener('DOMContentLoaded', async () => {
    // for states

    const stateUrl = 'http://localhost:8000/api/states/';

    try {
        const response = await fetch(stateUrl);

        if(!response.ok) {
            throw new Error('Response not ok');
        } else {
            const data = await response.json();
            let selectTag = document.querySelector('#state');

            for (let state of data.states){
                // console.log(state);
                let option = document.createElement('option');
                option.value = state.abbreviation;
                option.innerHTML = state.name;
                selectTag.appendChild(option);
            }

        }
    } catch (e) {
        console.error('error', e);
    }

    //for location
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
                console.log(newLocation);
            }
    });
});